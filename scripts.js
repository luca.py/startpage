/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */

/**
 * Search function
 */

const searchInput = document.querySelector("#searchbar > input")
const searchButton = document.querySelector("#searchbar > button")

const lookup = {"/":"/","deepl":"https://deepl.com/","reddit":"https://reddit.com/","maps":"https://maps.google.com/"}
const engine = "google"
const engineUrls = {
  deepl: "https://www.deepl.com/translator#-/-/",
  duckduckgo: "https://duckduckgo.com/?q=",
  ecosia: "https://www.ecosia.org/search?q=",
  google: "https://www.google.com/search?q=",
  startpage: "https://www.startpage.com/search?q=",
  youtube: "https://www.youtube.com/results?q=",
}

const isWebUrl = value => {
  try {
    const url = new URL(value)
    return url.protocol === "http:" || url.protocol === "https:"
  } catch {
    return false
  }
}

const getTargetUrl = value => {
  if (isWebUrl(value)) return value
  if (lookup[value]) return lookup[value]
  return engineUrls[engine] + value
}

const search = () => {
  const value = searchInput.value
  const targetUrl = getTargetUrl(value)
  window.open(targetUrl, "_self")
}

searchInput.onkeyup = event => event.key === "Enter" && search()
searchButton.onclick = search

/**
 * inject bookmarks into html
 */

const bookmarks = [{"id":"83en5nqRXt3bwlqA","label":"social media","bookmarks":[{"id":"vKwVxnVGfjVXQ9ze","label":"facebook","url":"https://www.facebook.com/"},{"id":"xAnVLGfhnLwhoD7x","label":"instagram","url":"https://www.instagram.com/"},{"id":"RLJejulQeAKkt8Bq","label":"reddit","url":"https://www.reddit.com/"}]},{"id":"dLx9Z1ZhQWHKloFH","label":"media","bookmarks":[{"id":"dfZhKtbCnjpPCivO","label":"youtube","url":"https://www.youtube.com/"},{"id":"NPGqxR8H7fDFISWz","label":"soap2dayhd","url":"https://ww3.soap2dayhd.co/home/"},{"id":"UodCSFlFDHRIe8ab","label":"shaheed4u","url":"https://shaheed4u.art/home2/"}]},{"id":"d8W9U8z4b1r86ew6","label":"programing","bookmarks":[{"id":"LEv77EJY9SN2KTi6","label":"gitlab","url":"https://gitlab.com/"},{"id":"m8jZf3ZCIarQDCkZ","label":"github","url":"https://github.com/"},{"id":"6TTq9kQfOwvrxsXL","label":"tryhackme","url":"https://tryhackme.com/dashboard"}]},{"id":"w05BY006eIbY40NJ","label":"sources","bookmarks":[{"id":"GhYhTnaEl2Ww6Z90","label":"icons","url":"https://feathericons.com/"},{"id":"AzoV0DAK0Q9nBeLv","label":"@startpage","url":"https://prettycoffee.github.io/startpage"},{"id":"kUj8tNcKMs7QoPLA","label":"author","url":"https://prettycoffee.github.io/"}]}]

const createGroupContainer = () => {
  const container = document.createElement("div")
  container.className = "bookmark-group"
  return container
}

const createGroupTitle = title => {
  const h2 = document.createElement("h2")
  h2.innerHTML = title
  return h2
}

const createBookmark = ({ label, url }) => {
  const li = document.createElement("li")
  const a = document.createElement("a")
  a.href = url
  a.innerHTML = label
  li.append(a)
  return li
}

const createBookmarkList = bookmarks => {
  const ul = document.createElement("ul")
  bookmarks.map(createBookmark).forEach(li => ul.append(li))
  return ul
}

const createGroup = ({ label, bookmarks }) => {
  const container = createGroupContainer()
  const title = createGroupTitle(label)
  const bookmarkList = createBookmarkList(bookmarks)
  container.append(title)
  container.append(bookmarkList)
  return container
}

const injectBookmarks = () => {
  const bookmarksContainer = document.getElementById("bookmarks")
  bookmarksContainer.append()
  bookmarks.map(createGroup).forEach(group => bookmarksContainer.append(group))
}

injectBookmarks()
